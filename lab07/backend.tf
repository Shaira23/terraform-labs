terraform {
  backend "s3" {
    bucket = "state-bucket-597429366363-us-east-1"
    key    = "policy/my-first-policy.tfstate"
    region = "us-east-1"
    # dynamodb_table = "terraform-lock"
  }
}

