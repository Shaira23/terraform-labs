variable "engine" {
    type = string
    default = "9.6.9"
    description = "Postgres db engine version"
}

variable "family" {
    type = string
    default = "postgres9.6"
    description = "Postgres db family version"
     
}

variable "major_version" {
    type = string
    default = "9.6"
}

variable "instance_class" {
    type = string
    default = "db.t2.micro"
}

